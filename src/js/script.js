"use strict";

const personalMovieDB = {
  count: 0,
  movies: {},
  actors: {},
  genres: [],
  isPrivate: false,
  database: document.querySelector(".database"),

  toggleVisibleMyDB: function() {
    this.database.replaceChildren();
    this.isPrivate = !this.isPrivate;
  },

  showMyDB: function() {
    if (this.database.hasChildNodes()) {
      this.database.replaceChildren();
      return;
    }

    if (!personalMovieDB.isPrivate) {
      let count = document.createElement("div");
      count.innerText = `count: ${this.count}`;
      let movies = document.createElement("div");
      movies.innerText = `movies: ${JSON.stringify(this.movies)}`;
      let actors = document.createElement("div");
      actors.innerText = `actors: ${JSON.stringify(this.actors)}`;
      let genres = document.createElement("div");
      genres.innerText = `genres: ${JSON.stringify(this.genres)}`;

      this.database.replaceChildren();
      [count, movies, actors, genres].forEach(item => this.database.append(item));
    }
  },

  writeYourMovies: function() {
    this.count = +prompt("Сколько фильмов вы уже посмотрели?");
    let answersGiven = 0;

    while (answersGiven < this.count) {
      let lastViewedMovie = prompt("Один из последних просмотренных фильмов?");
      if (lastViewedMovie === null || lastViewedMovie === "" || lastViewedMovie.length > 50) {
        alert("Повторите ввод");
        continue;
      }
      this.movies[lastViewedMovie] = +prompt("На сколько оцените его?");
      answersGiven++;
    }

    if (this.count < 10) {
      alert("Просмотрено довольно мало фильмов");
    } else if (10 <= this.count && this.count <= 30) {
      alert("Вы классический зритель");
    } else if (this.count > 30) {
      alert("Вы киноман");
    } else {
      alert("Произошла ошибка");
    }
  },

  writeYourGenres: function () {
    let genresGiven = 0;
    while (genresGiven < 3) {
      let genre = prompt(`Ваш любимый жанр под номером ${genresGiven + 1}`);
      if (genre === null || genre === "") {
        alert("Повторите ввод");
        continue;
      }
      this.genres.push(genre);
      genresGiven++;
    }

    this.genres.forEach((item, i) => {
      console.log(`Любимый жанр #${i + 1} - это ${item}`);
    });
  }
};

document.querySelector(".toggle-db").addEventListener("click", () => {
  personalMovieDB.toggleVisibleMyDB()
});
document.querySelector(".show-db").addEventListener("click", (event) => {
  personalMovieDB.showMyDB();
});
document.querySelector(".write-genres").addEventListener("click", () => {
  personalMovieDB.writeYourGenres();
});
document.querySelector(".write-movies").addEventListener("click", (event) => {
  let form = document.querySelector(".movies-form");
  let visibility = form.style.visibility;
  if (visibility === "visible") {
    form.style.visibility = "hidden";
  } else {
    form.style.visibility = "visible";
  }
});

document.querySelector(".add-movie-button").addEventListener("click", (event) => {
  event.preventDefault();
  let name = document.querySelector(`.movies-form input[type="text"]`).value;
  if (name === "") {
    return;
  }

  if (name.length > 21) {
    name = `${name.substring(0, 22)}...`;
  }

  let rate = document.querySelector(`.movies-form input[type="number"]`).value;
  if (rate === "") {
    return;
  }

  let movie = document.createElement("div");
  movie.innerText = `"${name}": ${rate}`;
  document.querySelector(".movies").append(movie);
  personalMovieDB.movies[name] = rate;
});
